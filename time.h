#pragma once

struct FilmTime
{
    unsigned int hour;
    unsigned int min;
    unsigned int sec;
    unsigned int GetSec();
    double GetMin();
    double GetHour();
    void Init(int h, int m, int s);
    void InputTime();
};
