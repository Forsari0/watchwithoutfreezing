#include <iostream>
#include "time.h"
#include "size.h"

using namespace std;

int main(void)
{
    Size speedRate;//kb/s
    Size filmSize;//Gb
    double Size;
    string Type;
    FilmTime t;
    ///InputData
    cout << "Enter your download speed in NUM TYPE/s (kb, Mb, Gb) pattern (ex: 1.2 Gb)\nspeedRate = ";
    cin >> Size >> Type;
    speedRate.InitSize(Size);
    speedRate.InitType(Type);
    cin.ignore();
    ///
    cout << "Enter size of film in NUM TYPE (kb, Mb, Gb) pattern (ex:427 Mb)\nfilmSize = ";
    cin >> Size >> Type;
    filmSize.InitSize(Size);
    filmSize.InitType(Type);
    cin.ignore();
    ///
    t.InputTime();
    ///
    cout << "Time data:\n" << "\tHours = " << t.GetHour() << endl <<
         "\tMin = " << t.GetMin() << endl <<
         "\tSec = " << t.GetSec() << endl;
    ///Calculation
    filmSize.Convert(filmSize.type, kb);//to kb
    double filmPlayKbInSec = filmSize.size / t.GetSec();
    std::cout << "Film Data:\n" << "\tfilmPlayKbInSec = " <<
              filmPlayKbInSec << endl;
    if ((static_cast<double>(speedRate.size)/static_cast<double>(filmPlayKbInSec)) > 1)
        std::cout << "Just click 'Play' button and watch your film!" << endl;
    else
    {
        double summOfTime = 0;
        double currentPossiblePlayback = 0;
        unsigned int j;
        for (j = 1; j < t.GetSec(); j++)
        {
            currentPossiblePlayback = ((speedRate.size*j)/filmPlayKbInSec);
            for(int i = j+currentPossiblePlayback; static_cast<unsigned int>(summOfTime) < t.GetSec();)
            {
                currentPossiblePlayback = ((speedRate.size*i)/filmPlayKbInSec);
                i = currentPossiblePlayback;
                if (currentPossiblePlayback <= 0)
                    break;
                else
                    summOfTime+=currentPossiblePlayback;
            }
            if (summOfTime >= t.GetSec())
                break;
            else
                summOfTime = 0;
        }
        std::cout << "Wait until film will be downloaded over "<< j << " seconds ("<< static_cast<double>(j/60.0) <<" minutes) or until file weight will be " <<
                  static_cast<double>(j*filmPlayKbInSec/1024.0) << " Mb or " <<
                  static_cast<double>(((j*filmPlayKbInSec)/1024.0/1024.0)) << " Gb and click 'Play'!\nEnjoy!" << endl;
    }
    std::cout << "\nPush ENTER button to continue..." << endl;
    cin.ignore();
    cin.get();
    return 0;
}
