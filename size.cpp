#include "size.h"
#include <math.h>

void Size::InitSize(double size_var)
{
    size = size_var;
}
void Size::InitType(std::string type_var)
{
    if (type_var == "kb")
        type = kb;
    else if (type_var == "Mb")
        type = Mb;
    else if (type_var == "Gb")
        type = Gb;
}

void Size::Convert(InfSize from, InfSize to)
{
    if (static_cast<int>(from) < static_cast<int>(to))
    {
        size/=static_cast<int>(to)*1024.0;
    }
    else if (static_cast<int>(from) > static_cast<int>(to))
    {
        size*=powl(1024.0, (static_cast<int>(from)-static_cast<int>(to)));
    }
}
