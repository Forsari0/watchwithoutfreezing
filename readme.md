﻿Watch Without Freezing
----------------------
**WatchWithoutFreezing** - small console tool, which can calculate minimal necessary time for download and watch any video without **'download ... wait 15 seconds...'** messages based on your download speed, video length and video size.

  - Supports kb, Mb, Gb representations of sizes and speeds
  - Easy to input data
  - ...and it works!

Licensed under [WTFPL] license.



[wtfpl]:http://www.wtfpl.net/

Some screenshots
![Alt text](https://bitbucket.org/Forsari0/watchwithoutfreezing/downloads/ozr9e3qmqn%202014-02-17%2020.32.30.png)
>Эта маленькая консольная программа будет полезна прежде всего тем пользователям, у кого до сих пор нет высокоскоростного подключения к сети. Или оно есть, но цены на него кусаются. В общем, неважно. А важно то, что из-за маленькой скорости скачивания иногда просто невозможно смотерть фильм\видеоролик онлайн из-за постоянных "подгрузок". Причина этому в том, что скорость загрузки видео меньше, чем скорость воспроизведения (kb/s). На самом деле, из этой ситуации есть достаточно простой выход - подождать, пока загрузится достаточный промежуток фильма. Но как определить этот промежуток времени? Конечно, можно это делать "на глаз" - 1\3 фильма загрузилась, значит, можно приступать его смотреть. А можно просто ввести в WWF данные и получить на выходе математически точный (без учтения колебаний скорости загрузки) результат. Выбор за вами.

*Forsari0, 2014*