#include "time.h"
#include <iostream>

void FilmTime::Init(int h, int m, int s)
{
    hour = h;
    min = m;
    sec = s;
}

void FilmTime::InputTime()
{
    int h, m, s;
    std::cout << "Enter duration of film in H M S format\ndurationFilm = ";
    std::cin >> h >> m >> s;
    Init(h, m, s);
}

unsigned int FilmTime::GetSec()
{
    return (hour*60*60)+(min*60)+(sec);
}

double FilmTime::GetMin()
{
    return (hour*60)+(min)+(static_cast<double>(sec)/60.0);
}

double FilmTime::GetHour()
{
    return (hour)+(static_cast<double>(min)/60.0)+(static_cast<double>(sec)/(60.0*60.0));
}
