#pragma once
#include <string>

enum InfSize {kb, Mb, Gb};

struct Size
{
    double size;
    InfSize type;
    void InitSize(double size_var);
    void InitType(std::string type_var);
    void Convert(InfSize from, InfSize to);
};
